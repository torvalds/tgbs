#!/bin/bash

htpasswd -cb /etc/httpd/.htpasswd $USERNAME $PASSWORD
chown root:apache /etc/httpd/.htpasswd
chmod 640 /etc/httpd/.htpasswd

mkdir /root/httpd
cp /etc/httpd/.htpasswd /root/httpd/.htpasswd

httpd
nginx

tail -f /var/log/httpd/*.log