# WebDAV container

You can run this container in following way. You can then access the WebDAV instance at `http://localhost:8888/webdav`. Internally the folder `/var/webdav` is used as WebDAV root. You can also upload your file to the webdav: curl -T yourfile -u username:password 'http://localhost:8888/webdav/yourfile'

```
docker run -d -e USERNAME=test -e PASSWORD=test -p 8888:80 morrisjobke/webdav
```

