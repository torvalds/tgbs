## Docker化监控系统

由以下组件构成:
1. Telegraf

    Telegraf is an agent written in Go for collecting, processing, aggregating, and writing metrics.

    entry: https://github.com/influxdata/telegraf

2. Graphite

    Graphite consists of 3 software components:

    1. carbon - a Twisted daemon that listens for time-series data
    2. whisper - a simple database library for storing time-series data (similar in design to RRD)
    3. graphite webapp - A Django webapp that renders graphs on-demand using Cairo

    entry: https://github.com/graphite-project

3. Graphite-beacon

    Simple alerting system for Graphite metrics.

    entry: https://github.com/klen/graphite-beacon

4. Grafana

    Grafana is an open source, feature rich metrics dashboard and graph editor for Graphite, Elasticsearch, OpenTSDB, Prometheus and InfluxDB.

    entry: https://github.com/grafana/grafana

5. Slack

    A tool for team communication. Provide restful api.

    entry: https://slack.com/

## Example

    example目录下是一个完整的例子,在该目录下(为防止污染git目录,也可以将该目录拷到本机的其他地方,因为config目录是必须的)执行

    docker-compose up -d

    使用 docker-compose logs -f 查看日志,docker ps 查看服务是否都启动成功